var express = require('express'),
    bodyParser = require('body-parser'),
    mongoose = require('mongoose'),
    Project = require('./models/project'),
    app     = express();

//route directories
var indexRoutes = require('./routes/index'),
    roadmapRoutes = require('./routes/roadmap'),
    projectRoutes = require('./routes/projects');



mongoose.connect("mongodb://sebasslash://#Srivera18@ds139994.mlab.com:39994/map-website");


app.use(express.static(__dirname +"/public"));
app.use('/node_modules',express.static(__dirname +"/node_modules"));
app.use(bodyParser.urlencoded({extended:true}));

app.use(roadmapRoutes);
app.use(projectRoutes);
app.use(indexRoutes);


//Heroku Deployment
// app.listen(process.env.PORT, process.env.IP, function(){
//     console.log("YAP Website has started");
//     console.log("Welcome to the Youth Action Project Website");
//     console.log("=============================================")
//     console.log("Empowering the future for a brighter today");
// });

//Development deployment
app.listen(3000, function(){
    console.log("YAP Website has started");
    console.log("Welcome to the Youth Action Project Website");
    console.log("=============================================")
    console.log("Empowering the future for a brighter today");
});
