var mongoose = require('mongoose');



//Add Tags Here
var tagSchema = new mongoose.Schema({
    environment = Boolean,
    climate = Boolean,
    clean = Boolean,
    energy = Boolean,
    citydev = Boolean,
    poverty = Boolean,
    education = Boolean,
    policy = Boolean,
    empowerment = Boolean,
    building = Boolean,
    joining = Boolean,
    voting  = Boolean
});


module.exports = mongoose.model("Tag", tagSchema);
