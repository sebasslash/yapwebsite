var mongoose = require('mongoose');

var projectSchema = new mongoose.Schema({
    title:String,
    org: String,
    content: String,
    desc: String,
    tag: String,
    img: String,
    website: String,
    date: Number
});

module.exports = mongoose.model("Project", projectSchema);
