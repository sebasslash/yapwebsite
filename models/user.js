var mongoose = require('mongoose'),
    passportLocal = require('passport-local-mongoose');


var userSchema = new mongoose.Schema({
    name: String,
    username: String,
    password: String,
    email: String
});

userSchema.plugin(passportLocal);

module.exports = mongoose.model("User", userSchema);
