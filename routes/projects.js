var express = require('express'),
    router  = express.Router(),
    Project = require('../models/project'),
    ProjectsCtrl = require('../controllers/project.controller');


router.get("/projects", ProjectsCtrl.findAllProjects);
router.post("/projects", ProjectsCtrl.addProject);
router.get("/projects/environment", ProjectsCtrl.findEnvironmentProjects);


module.exports = router;
