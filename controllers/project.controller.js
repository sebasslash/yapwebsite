var Project = require('../models/project');

module.exports.addProject = function(req,res){
    Project.create(req.body.project, function(err,project){
        if(err){
            console.log(err);
        }else{
            project.save();
        }
    });
};

module.exports.findAllProjects = function(req,res){
    Project.find({}, function(err,projects){
        if(err){
            console.log(err);
        }else{
            res.json(projects);
        }
    });
};

module.exports.findProjectById = function(req,res){
    Project.findById(req.params._id, function(err, project){
        if(err){
            console.log(err);
        }else{
            res.json(project);
        }
    });
};


//Return all projects with the Environment tag
module.exports.findEnvironmentProjects = function(req,res){
    Project.find({tag:"environment"}, function(err,env_projects){
        if(err){
            console.log(err);
        }else{
            res.json(env_projects);
        }
    });
};
module.exports.findCityProjects = function(req,res){
    Project.find({tag:"citydevelopment"}, function(err, city_projects){
        if(err){
            console.log(err);
        }else{
            res.json(city_projects);
        }
    })
};
