angular.module('map').controller("ContactController", ContactController);

function ContactController(){
    var vm = this;
    vm.title = "Contact";
    vm.email = "sebastian.rivera@jcxsoftware.com";
    vm.representative = "Sebastian Rivera";
    vm.phone = "305-608-4280";
    vm.mailbox = "11101 NW 44th Terrace";
}
