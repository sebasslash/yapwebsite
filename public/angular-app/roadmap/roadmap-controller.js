angular.module('map').controller("RoadmapController", RoadmapController);

function RoadmapController($http){
    var vm = this;
    var submitButton = $('#submit');
    var optionsList = [vm.category, vm.subcategory, vm.action,vm.name,vm.email];
    submitButton.click(function(){
        $http.post('/roadmap').then(function(res){
            console.log(res);
        });
    });
}
