angular.module('map', ['ngRoute'])
.config(config)
.controller('MainController', MainController);

function config($routeProvider){
    $routeProvider
    .when('/', {
        templateUrl:'angular-app/home.html',
        controller: 'MainController',
        controllerAs:'vm'
    }).when('/about', {
        templateUrl:'angular-app/about.html',
        controller: 'AboutController',
        controllerAs:'vm'
    }).when('/contact', {
        templateUrl:'angular-app/contact.html',
        controller: 'ContactController',
        controllerAs: 'vm'
    }).when('/projects', {
        templateUrl:'angular-app/project-view/projects.html'
    })
    .when('/projects/add', {
        templateUrl:'angular-app/project-view/add-project.html',
        controller:'ProjectController',
        controllerAs:'vm'
    }).when('/opportunities', {
        templateUrl:'angular-app/opportunities.html',
        controller: 'OpportunitiesController',
        controllerAs: 'vm'
    }).when('/education', {
        templateUrl:'angular-app/education.html',
        controller: 'EducationController',
        controlleerAs: 'vm'
    });

}

//This is the controller that controls the main page;
function MainController(){
    //vm for view model
    var vm = this;
    vm.title = "Miami Action Project!";


}
